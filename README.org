* What is this?

  This is little example on calling rust from python using [[https://github.com/PyO3/pyo3/][pyo3]].



* How to install
  Create a virtual environment in python. Set you =rustc= version to nightly by
  #+BEGIN_SRC sh
rustup override set nightly
  #+END_SRC
  because =pyo3= depends on package which requires =nightly= features.

  Now change to the virtual environment and run
  #+BEGIN_SRC sh
python setup.py develop
  #+END_SRC
  Which will install =rust_py= package.


* How to run
  I implemented some simple functions in [[./src/lib.rs][this file]]. You can check [[./example.py][file]] for
  python usage. Or run

  #+BEGIN_SRC sh
python example.py
  #+END_SRC
  and you can python calling rust functions.
