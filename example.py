from rust_py import rust_py
import numpy as np


print("calling sum_as_usize function from rust in python")
print(rust_py.sum_as_usize(10, 20))

print("calling sum_as_string function from rust in python")
print(rust_py.sum_as_string(1, 234))

print("calling cumulative_sum function from rust in python")
print(rust_py.cumulative_sum(234))

print("calling vec_sum function from rust in python")
print(rust_py.vec_sum(np.array([1., 2., 3.])))
