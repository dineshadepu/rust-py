#![feature(specialization)]

use pyo3::prelude::*;
use pyo3::wrap_function;


#[pyfunction]
fn sum_as_usize(a: usize, b: usize) -> PyResult<usize>{
    Ok(a + b)
}

#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String>{
    Ok((a + b).to_string())
}

#[pyfunction]
fn cumulative_sum(a: usize) -> PyResult<usize>{
    let mut sum = 0;
    for i in 0..a{
        sum += i;
    }
    Ok(sum)
}

#[pyfunction]
fn vec_sum(a: &[f32]) -> PyResult<usize>{
    let mut sum = 0;
    for i in 0..a.len(){
        sum += i;
    }
    Ok(sum)
}

#[pymodinit]
fn rust_py(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_function!(sum_as_string))?;
    m.add_function(wrap_function!(sum_as_usize))?;
    m.add_function(wrap_function!(cumulative_sum))?;
    m.add_function(wrap_function!(vec_sum))?;

    Ok(())
}
